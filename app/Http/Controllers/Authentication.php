<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use App\Models\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Authentication extends Controller
{
    private $userModel;
    public function __construct()
    {
        $this->userModel = new UserModel();
    }
    public function login()
    {
        return view('login.login');
    }
    public function sigin()
    {
        return view('sigin.sigin');
    }
    public function startSigin(UserRequest $request)
    {
        $this->userModel->insertUser($request);
        session(['sigin_success' => 'Đăng ký tài khoản thành công']);
        return view('login.login');
    }

    public function startLogin(LoginRequest $request)
    {
        $path = $this->userModel->checkLogin($request);
        return view("$path");
    }
    public function logOut(Request $request)
    {
        $data = $this->userModel->logOut($request);
        return redirect(route("$data"));
    }
}