<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Coin;
use App\Models\HandleCoin;
use App\Models\JobCategory;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Models\UserModel;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    private $userModel;
    private $job;
    private $coin;
    public function __construct()
    {
        $this->userModel = new UserModel();
        $this->job = new JobCategory();
        $this->coin = new Coin();
    }
    public function userAdmin(Request $request)
    {
        $data = $this->userModel->getUserRecruit($request->id);
        return view('admin.list_user_admin', compact('data'));
    }
    public function userRecuit(Request $request)
    {
        $data = $this->userModel->getUserRecruit($request->id);
        return view('admin.list_user_recruit', compact('data'));
    }
    public function userNormal(Request $request)
    {
        $data = $this->userModel->getUserRecruit($request->id);
        return view('admin.list_user_normal', compact('data'));
    }
    public function listJob()
    {
        $data = $this->job->allJob();
        //dd($data);
        return view('admin.list_job', compact('data'));
    }
    public function addAdminAccount()
    {
        $data = ['id'=>'', 'user_name'=>'', 'password'=>'', 'name'=>'', 'email'=>'', 'phone_number'=>'','major'=>'', 'age'=>'', 'level'=>''];
        $url = "";
        return view('admin.add_update_admin', compact('data', 'url'));
    }
    public function startAddAdmin(UserRequest $request)
    {      
        if(empty($request->id))
        {
            $this->userModel->insertUser($request);
            return redirect(route('user_admin', ['id' => 1]));
        }
        else{
            $this->userModel->updateUser($request);
            session(['update_success' => 'Update thành công']);
            return redirect($request->url);
        }

    }
    public function deleteUser(Request $request)
    {
        $this->userModel->delUser($request->id);
        return redirect(url()->previous());
    }
    public function deleteJob(Request $request)
    {
        $this->job->delJob($request->id);
        return redirect(url()->previous());
    }
    public function updateUser(Request $request)
    {
        $data = $this->userModel->getUser($request->id);
        $url = url()->previous();
        return view('admin.add_update_admin', compact(['data', 'url']));
    }
    public function handleCoin()
    {
        $data = HandleCoin::with('user')->where('status', 'Pending')->get();
        return view('admin.handle_coin', compact('data'));
    }
    public function updateCoin(Request $request)
    {
        $this->coin->updateCoin($request);
        return redirect()->route('handle_coin');
    }
}