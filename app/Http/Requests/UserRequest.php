<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'userName' => 'required|min:6|max:191|unique:mysql.users,user_name,'.request()->id,
            'pass' => 'required|min:8|max:191',
            'cfpass' => 'required|same:pass',
            'name' => 'required|max:191',
            'email' => 'required|email:rfc|unique:mysql.users,email,'.request()->id,
            'phoneNum' => 'required',
            'major' => 'required',
            'age' => 'required|integer',
        ];
    }
    public function messages()
    {
        return [
            'userName.required' => 'Vui lòng nhập đầy đủ user name',
            'userName.min' => 'User name tối thiểu là 6 ký tự',
            'userName.max' => 'User name tối đa là 191 ký tự',
            'userName.unique' => 'Tài khoản đã tồn tại',
            'pass.required' => 'Vui lòng nhập đầy đủ mật khẩu',
            'pass.min' => 'Mật khẩu tối thiểu là 8 ký tự',
            'cfpass.same' => 'Password và Confirm Password không khớp',
            'pass.max' => 'Mật khẩu tối đa là 191 ký tự',
            'name.required' => 'Vui lòng nhập đầy đủ họ tên',
            'email.required' => 'Vui lòng nhập đầy đủ địa chỉ Email',
            'email.unique' => 'Email này đã tồn tại',
            'email.email' => 'Định dạng email vừa nhập không đúng',
            'phoneNum.required' => 'Vui lòng nhập đầy đủ số điện thoại',
            'major.required' => 'Vui lòng nhập nghề nghiệp',
            'age.required' => 'Vui lòng nhập tuổi',
            'age.integer' => 'Tuổi phải là số nguyên',
        ];   
    }
}