<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HandleCoin extends Model
{
    use HasFactory;
    protected $table = "handle_coins";
    protected $fillable = ['id_recruit', 'coin_number', 'status'];
    public function addHandleCoin($request)
    {
        $coin = new HandleCoin();
        $coin->id_recruit = $request->id_recruit;
        $coin->coin_number = $request->cost/1000;
        $coin->status = $request->status;
        $coin->save();
    }
    public function user()
    {
        return $this->belongsTo(UserModel::class, 'id_recruit', 'id');
    }
}