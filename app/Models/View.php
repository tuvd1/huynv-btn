<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    use HasFactory;
    protected $table = "views";
    protected $fillable = ['id_recruit', 'id_user'];
    public function updateView($request)
    {
        View::updateOrCreate(
            [
                'id_recruit' => $request->id_recruit,
                'id_user' => $request->id
            ],
            ['id_recruit' => $request->id_recruit],
        );
    }
    public function user()
    {
        return $this->belongsTo(UserModel::class, 'id', 'id_user');
    }
}