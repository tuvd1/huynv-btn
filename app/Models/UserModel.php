<?php

namespace App\Models;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;


class UserModel extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = "users";
    protected $fillable =['user_name', 'password', 'name', 'email', 'phone_number', 'level', 'age', 'major'];
    public function insertUser($request)
    {
        $user = new UserModel();
        $user->user_name = $request->userName;
        $user->password = bcrypt($request->pass);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone_number = $request->phoneNum;
        $user->major = $request->major;
        $user->age = $request->age;
        $user->level = $request->level;
        $user->save();
    }

    public function checkLogin($request)
    {
        if(Auth::attempt(['user_name' => $request->username, 'password' => $request->pass, 'level' => 1]))
        {
            session(['login_success' => 'Đănh nhập thành công']);
            session(['user_name_admin' => $request->username]);
            return 'Admin.admin';
        }
        if(Auth::attempt(['user_name' => $request->username, 'password' => $request->pass, 'level' => 2]))
        {
            session(['login_success' => 'Đănh nhập thành công']);
            session(['user_name_recruit' => $request->username]);
            return 'Recruit.user_recruit';
        }
        if(Auth::attempt(['user_name' => $request->username, 'password' => $request->pass, 'level' => 3]))
        {
            session(['login_success' => 'Đănh nhập thành công']);
            session(['user_name_normal' => $request->username]);
            return 'Normal.user_normal';
        }
        else{
            session(['err_login' => 'Sai tài khoản hoặc mật khẩu']);
            return "Login.login";
        }
    }
    public function logOut($request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return "login";
    }

    public function getUserRecruit($id)
    {
        return UserModel::where('level', $id)->get();
    }
    public function delUser($id)
    {
        $user = UserModel::find($id);
        $user->delete();
    }
    public function updateUser($request)
    {
        $user = UserModel::find($request->id);
        $user->user_name = $request->userName;
        $user->password = bcrypt($request->pass);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone_number = $request->phoneNum;
        $user->major = $request->major;
        $user->age = $request->age;
        $user->level = $request->level;
        $user->save();
    }
    public function getUser($id)
    {
        return UserModel::find($id);
    }
    public function job_categorie()
    {
        return $this->hasMany(JobCategory::class, 'id_recruit', 'id');
    }
    public function coin()
    {
        return $this->hasOne(Coin::class, 'id_recruit', 'id');
    }
    public function handle_coin()
    {
        return $this->hasMany(Coin::class, 'id_recruit', 'id');
    }
    public function view()
    {
        return $this->hasMany(View::class, 'id_user', 'id');
    }

}