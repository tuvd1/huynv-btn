<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    use HasFactory;
    protected $table = "coins";
    protected $fillable = ['id_recruit', 'coin_number'];
    public function user()
    {
        return $this->belongsTo(UserModel::class, 'id', 'id_recruit');
    }
    public function updateCoin($request)
    {
        $coin = Coin::where('id_recruit', $request->id)->get(); 
        if($coin->count() > 0){
            Coin::updateOrCreate(
                ['id_recruit' => $request->id],
                [
                    'coin_number' => $coin[0]->coin_number + $request->coin_number
                ]
            );
            HandleCoin::where('id_handle', $request->id_handle)->update(['status' => 'Accepted']);
        }
        else{
            Coin::updateOrCreate(
                [
                    'id_recruit' => $request->id,
                    'coin_number' => $request->coin_number
                ],
                ['id_recruit' => $request->id],
            );
            HandleCoin::where('id_handle', $request->id_handle)->update(['status' => 'Accepted']);
        }
    }
    public function coinView($request)
    {
        $coin = Coin::where('id_recruit', $request->id_recruit)->get();
        if($coin->count() > 0){
            Coin::updateOrCreate(
                ['id_recruit' => $request->id_recruit],
                [
                    'coin_number' => $coin[0]->coin_number - 10
                ]
            );
        }
        else{
            Coin::updateOrCreate(
                [
                    'id_recruit' => $request->id_recruit,
                    'coin_number' => 0
                ],
                ['id_recruit' => $request->id_recruit],
            );
        }
    }
}