<?php

use App\Http\Controllers\Authentication;
use App\Http\Controllers\RecruitController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserNormalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [Authentication::class, 'login'])->name('login');
Route::get('/sigin', [Authentication::class, 'sigin'])->name('sigin');
Route::post('/startSigin', [Authentication::class, 'startSigin'])->name('startSigin');
Route::post('/startLogin', [Authentication::class, 'startLogin'])->name('startLogin');
Route::get('/log_out', [Authentication::class, 'logOut'])->name('log_out');

Route::group(['prefix' => 'admin'], function(){
    Route::middleware(['admin', 'auth'])->group(function () {
        Route::get('/user_recuit/{id}', [AdminController::class, 'userRecuit'])->name('user_recuit');
        Route::get('/add_admin_account', [AdminController::class, 'addAdminAccount'])->name('add_admin_account');
        Route::get('/user_admin/{id}', [AdminController::class, 'userAdmin'])->name('user_admin');
        Route::get('/user_normal/{id}', [AdminController::class, 'userNormal'])->name('user_normal');
        Route::get('/list_jobAdmin', [AdminController::class, 'listJob'])->name('list_jobAdmin');
        Route::post('/start_add_admin', [AdminController::class, 'startAddAdmin'])->name('start_add_admin');
        Route::delete('/delete_user/{id}', [AdminController::class, 'deleteUser'])->name('delete_user');
        Route::delete('/delete_job/{id}', [AdminController::class, 'deleteJob'])->name('delete_job');
        Route::get('/update/{id}', [AdminController::class, 'updateUser'])->name('update_user');
        Route::get('/handle_coin', [AdminController::class, 'handleCoin'])->name('handle_coin');
        Route::get('/update_coin/{id}/{id_handle}/{coin_number}', [AdminController::class, 'updateCoin'])->name('update_coin');
    });
});

Route::group(['prefix' => 'recruit'], function(){
    Route::middleware(['recruit', 'auth'])->group(function () {
        Route::get('/list_job', [RecruitController::class, 'listJob'])->name('list_job');
        Route::get('/add_job', [RecruitController::class, 'addJob'])->name('add_job');
        Route::post('/start_add_job', [RecruitController::class, 'startAddJob'])->name('start_add_job');
        Route::get('/update_job/{id}', [RecruitController::class, 'updateJob'])->name('update_job');
        Route::put('/start_update_job', [RecruitController::class, 'startUpdateJob'])->name('start_update_job');
        Route::delete('/delete_job/{id}', [AdminController::class, 'deleteJob'])->name('delete_job');
        Route::get('/pay_coin', [RecruitController::class, 'payCoin'])->name('pay_coin');
        Route::post('/pay_in', [RecruitController::class, 'payIn'])->name('pay_in');
        Route::get('/list_career', [RecruitController::class, 'listCareer'])->name('list_career');
        Route::get('/list_user/{major}', [RecruitController::class, 'listUserNormal'])->name('list_user');
        Route::get('/view_user/{id}/{id_recruit}', [RecruitController::class, 'viewUser'])->name('view_user');
    });
});

Route::group(['prefix' => 'normal'], function(){
    Route::middleware(['normal', 'auth'])->group(function () {
        Route::get('/list_job', [UserNormalController::class, 'listJobType'])->name('user_normal_list_job');
        Route::get('/list/{career}', [UserNormalController::class, 'listJobByCareer'])->name('list');
        Route::get('/upload', [UserNormalController::class, 'upload'])->name('upload');
        Route::post('/start_upload', [UserNormalController::class, 'startUpload'])->name('start_upload');
        Route::get('/view_cv', [UserNormalController::class, 'viewCV'])->name('view_cv');
    });
});