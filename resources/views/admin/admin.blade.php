@extends('layout.main')

@section('tittle')
    <title>Admin Page</title>
@endsection

@section('user_name')
    <a href="" class="d-block">{{Session::get('user_name_admin')}}</a>
@endsection

@section('menu')
    @include('blocks/menu_admin')
@endsection

@section('content')
    @include('blocks.profile')                                   
@endsection

