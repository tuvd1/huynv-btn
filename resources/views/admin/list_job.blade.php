@extends('layout.main')

@section('tittle')
    <title>List Job</title>
@endsection

@section('user_name')
    <a href="" class="d-block">{{Session::get('user_name_admin')}}</a>
@endsection

@section('menu')
    @include('blocks/menu_admin')
@endsection

@section('content')
<table id="example2" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name Job</th>
            <th>User Create</th>
            <th>User Email</th>
            <th>User Major</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach( $data as $value)
        <tr>
            <th scope="row">{{ $value["id"]; }}</th>
            <td>{{ $value["name_job"] }}</td>
            <td>{{ $value["name"]; }}</td>
            <td>{{ $value["email"]; }}</td>
            <td>{{ $value["major"]; }}</td>
            <td>
                <form action="{{route('delete_job', ['id' => $value['id']])}}" method="post">
                    @csrf
                    @method('delete')
                    <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection