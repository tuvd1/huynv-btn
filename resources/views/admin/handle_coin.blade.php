@extends('layout.main')

@section('tittle')
    <title>Handle Coin</title>
@endsection

@section('user_name')
    <a href="" class="d-block">{{Session::get('user_name_admin')}}</a>
@endsection

@section('menu')
    @include('blocks/menu_admin')
@endsection

@section('content')
    <div class="card-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Id Recruit</th>
                    <th>User Name</th>
                    <th>Coin Number</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach( $data as $value)
                <tr>
                    <th scope="row">{{$value->user["id"]; }}</th>
                    <td>{{$value->user["user_name"] }}</td>
                    <td>{{$value["coin_number"]; }}</td>
                    <td>
                        <form action="{{route('update_coin', ['id' => $value->user['id'], 'id_handle' => $value['id_handle'], 'coin_number' => $value["coin_number"]])}}" method="post">
                            @csrf
                            @method('get')
                            <button type="submit" class="btn btn-primary">Accept</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

