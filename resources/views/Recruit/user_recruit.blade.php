@extends('layout.main')

@section('tittle')
    <title>Recruit Page</title>
@endsection

@section('user_name')
    <a href="" class="d-block">{{Session::get('user_name_recruit')}}</a>
@endsection

@section('menu')
    @include('blocks/menu_recruit')
@endsection

@section('content')
    @include('blocks.profile')                                   
@endsection

