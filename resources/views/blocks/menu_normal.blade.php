<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
    data-accordion="false">
    <li class="nav-item">
        <a href="{{route('user_normal_list_job')}}" class="nav-link">
            <i class="nav-icon far fa-envelope"></i>
            <p>
            List Job
            <i class="fas fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            @if (isset($data))
                @foreach($data as $value)
                <li class="nav-item">
                    <a href="{{route('list', ['career' => $value['career']])}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>{{$value['career']}}</p>
                    </a>
                </li>
                @endforeach
            @endif
        </ul>
    </li>
    <li class="nav-item">
        <a href="{{route('view_cv')}}" class="nav-link {{url()->current() == 'http://btn.test/normal/view_cv' ? 'active' : ''}}">
            <i class="nav-icon fas fa-copy"></i>
            List User View CV
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('upload')}}" class="nav-link {{url()->current() == 'http://btn.test/normal/upload' ? 'active' : ''}}">
            <i class="nav-icon fas fa-copy"></i>
            Upload CV
        </a>
    </li>
</ul>