<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
    data-accordion="false">
        <li class="nav-item">
            <a href="{{route('user_admin', ['id' => 1])}}" class="nav-link {{url()->current() == 'http://btn.test/admin/user_admin/1' ? 'active' : ''}}">
                <i class="nav-icon fas fa-copy"></i>
                List User Admin
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('user_recuit', ['id' => 2])}}" class="nav-link {{url()->current() == 'http://btn.test/admin/user_recuit/2' ? 'active' : ''}}">
                <i class="nav-icon fas fa-copy"></i>
                List User Recruit
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('user_normal', ['id' => 3])}}" class="nav-link {{url()->current() == 'http://btn.test/admin/user_normal/3' ? 'active' : ''}}">
                <i class="nav-icon fas fa-copy"></i>
                List User Normal
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('list_jobAdmin')}}" class="nav-link {{url()->current() == 'http://btn.test/admin/list_jobAdmin' ? 'active' : ''}}">
                <i class="nav-icon fas fa-copy"></i>
                List Job
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('add_admin_account')}}" class="nav-link {{url()->current() == 'http://btn.test/admin/add_admin_account' ? 'active' : ''}}">
                <i class="nav-icon fas fa-copy"></i>
                Add New Admin Account
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('handle_coin')}}" class="nav-link {{url()->current() == 'http://btn.test/admin/handle_coin' ? 'active' : ''}}">
                <i class="nav-icon fas fa-copy"></i>
                Handle Coin
            </a>
        </li>
    </ul>