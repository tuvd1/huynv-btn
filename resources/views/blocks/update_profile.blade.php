<div class="container">
	<div class="row">
		<div class="col-md-9">
		    <div class="card">
		        <div class="card-body">
		            <div class="row">
		                <div class="col-md-12">
		                    <h4>Your Profile</h4>
		                    <hr>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-md-12">
		                    <form action="{{route('update_profile')}}" method="POST">
                                @csrf
                                @method('put')
                                <div class="form-group row">
                                    <label for="username" class="col-4 col-form-label">User Name</label> 
                                    <div class="col-8">
                                    <input  value="{{auth()->user()->user_name}}" id="username" name="userName" placeholder="Username" class="form-control here" required="required" type="hidden" >
                                    <label for="username" class="col-4 col-form-label">{{auth()->user()->user_name}}</label> 
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Password</label> 
                                    <div class="col-8">
                                        <input value="{{auth()->user()->password}}" id="name" name="pass" placeholder="Password" class="form-control here" type="password">
                                        @error('pass')
                                            <span style="color: red;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lastname" class="col-4 col-form-label">Confirm Password</label> 
                                    <div class="col-8">
                                        <input value="{{auth()->user()->password}}" id="lastname" name="cfpass" placeholder="Confirm Password" class="form-control here" type="password">
                                        @error('cfpass')
                                            <span style="color: red;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="text" class="col-4 col-form-label">Name</label> 
                                    <div class="col-8">
                                        <input value="{{auth()->user()->name}}" id="text" name="name" placeholder="Name" class="form-control here" required="required" type="text">
                                        @error('name')
                                            <span style="color: red;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-4 col-form-label">Phone Number</label> 
                                    <div class="col-8">
                                        <input value="{{auth()->user()->phone_number}}" id="email" name="phoneNum" placeholder="Email" class="form-control here" required="required" type="text">
                                        @error('phoneNum')
                                            <span style="color: red;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-4 col-form-label">Email</label> 
                                    <div class="col-8">
                                        <input value="{{auth()->user()->email}}" id="email" name="email" placeholder="Email" class="form-control here" required="required" type="text">
                                        @error('email')
                                            <span style="color: red;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="website" class="col-4 col-form-label">Age</label> 
                                    <div class="col-8">
                                        <input value="{{auth()->user()->age}}" id="email" name="age" placeholder="Age" class="form-control here" required="required" type="text">
                                        @error('age')
                                            <span style="color: red;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="publicinfo" class="col-4 col-form-label">Major</label> 
                                    <div class="col-8">
                                        <input value="{{auth()->user()->major}}" id="email" name="major" placeholder="Major" class="form-control here" required="required" type="text">
                                        <input type="hidden" name="id" value="{{auth()->user()->id}}">
                                        @error('major')
                                            <span style="color: red;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="publicinfo" class="col-4 col-form-label">User Type</label>
                                    @if (auth()->user()->level == 1)
                                        <div class="col-8">                           
                                            <select name="level" id="">
                                                <option value="1">User Admin</option>
                                            </select>
                                        </div>
                                    @else
                                        <div class="col-8">                           
                                            <select name="level" id="">
                                                <option value="2">User Recruiter</option>
                                                <option value="3">User Normal</option>
                                            </select>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <div class="offset-4 col-8">
                                    <button name="submit" type="submit" class="btn btn-primary">Update My Profile</button>
                                    </div>
                                </div>
                            </form>
		                </div>
		            </div>
		            
		        </div>
		    </div>
		</div>
	</div>
</div>