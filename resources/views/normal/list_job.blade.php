@extends('layout.main')

@section('tittle')
    <title>List Job</title>
@endsection

@section('user_name')
    <a href="" class="d-block">{{Session::get('user_name_normal')}}</a>
@endsection

@section('menu')
    @include('blocks/menu_normal')
@endsection

@section('content')
    <table id="example2"
    class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name Job</th>
            <th>Career</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @if (isset($data2))
        <@foreach( $data2 as $value)
        <tr>
            <th scope="row">{{$value["id"]}}</th>
            <td>{{$value["name_job"] }}</td>
            <td>{{$value["career"] }}</td>
            <td>
                <form action="" method="post">
                    @csrf
                    @method('get')
                    <button type="submit" class="btn btn-primary">Join</button>
                </form>
            </td>
        </tr>
        @endforeach
        @endif
    </tbody>
    </table>
@endsection