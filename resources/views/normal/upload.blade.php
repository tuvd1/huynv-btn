@extends('layout.main')

@section('tittle')
    <title>List Job</title>
@endsection

@section('user_name')
    <a href="" class="d-block">{{Session::get('user_name_normal')}}</a>
@endsection

@section('menu')
    @include('blocks/menu_normal')
@endsection

@section('content')
    <form action="{{route('start_upload')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
        <div class="form-group">
            <input type="file" class="form-control"
                name="cv" id="exampleInputEmail1">
            <input class="form-control" type="date"
                name="date">
            <input type="hidden" name="id"
                value="{{Auth::id()}}">
            <input type="hidden" name="name" value="{{Auth::user()}}">
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">
                Upload</button>
        </div>
    </form>
@endsection